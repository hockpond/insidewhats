import React from 'react'
import ReactDOM from 'react-dom'

import Website from './components/Website/Website'

ReactDOM.render(<Website />, document.getElementById('root'))