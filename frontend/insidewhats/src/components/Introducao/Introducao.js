import React from 'react'
import './Introducao.css'
import Logo from '../Logo/Logo'

const url = "http://localhost:3000"

function Introducao() {
    return(
        <div className="Introducao">
            {
                //Header
            }
            <img className="background" alt=""></img>
            <Logo />
            <a href={url}><p className="mobileMarketing">Mobile Marketing</p></a>
            <a href={url}><p className="vantagens">Vantagens</p></a>
            <a href={url}><p className="recursos">Recursos</p></a>
            <button class="botaoContato"><strong>Contato</strong></button>

            {
                //Corpo
            }
            <h1 className="slogan">Seus clientes ao seu alcance</h1>
            <p className="descricao"><strong>InsideWhats</strong> é uma plataforma alinhada com as diretrizes e políticas do WhatsApp. Fazemos disparos apenas para usuários que <strong>manifestaram interesse</strong> em sua empresa.</p>
            <button className="botaoBanner"><strong>Lorem ipsum dor sit amet</strong></button>
            <p>aaaaaaaaaaaaa</p>
        </div>
    )
}

export default Introducao