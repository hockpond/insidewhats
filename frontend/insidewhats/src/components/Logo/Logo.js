import React from 'react'
import './Logo.css'

function Logo() {
    return(
        <div className="logo">InsideWhats</div>
    )
}

export default Logo